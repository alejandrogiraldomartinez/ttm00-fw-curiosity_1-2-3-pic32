/*
 * File:   main.c
 * Author: user
 *
 * Created on July 9, 2019, 10:16 AM
 */

#include "main.h"


void LED_on(uint8_t LED);
void LED_off(uint8_t LED);
void LED_toggle(uint8_t LED);
void LED_alternate(uint8_t LED);

uint8_t LED = 0;
void main(void) {    
    
    OpenCoreTimer(CORE_TICK_RATE);//Configures the 32-Bit CPU Timer registers
    mConfigIntCoreTimer ( (CT_INT_ON | CT_INT_PRIOR_3 | CT_INT_SUB_PRIOR_0 ) ); 
    INTConfigureSystem (INT_SYSTEM_CONFIG_MULT_VECTOR);
    INTEnableInterrupts ();
    
    led_struct_t LED1_struct;
    LED1_struct.c_LED_port = 'E';
    LED1_struct.u8_LED_pin = BIT_4;
    LED1_struct.u8_LED_status = LED_OFF;
    
    led_struct_t LED2_struct;
    LED2_struct.c_LED_port = 'E';
    LED2_struct.u8_LED_pin = BIT_6;
    LED2_struct.u8_LED_status = LED_OFF;
    
    led_struct_t LED3_struct;
    LED3_struct.c_LED_port = 'E';
    LED3_struct.u8_LED_pin = BIT_7;
    LED3_struct.u8_LED_status = LED_OFF;
    
    UART1_init(115200);
    UART1_write_string("[PIC]: Initializing...\r\n");
    
    mPORTDSetPinsDigitalIn(BIT_6); // USER SW
    
    mPORTBSetPinsDigitalOut(BIT_10 | BIT_3 | BIT_2);
    mPORTESetPinsDigitalOut(BIT_4 | BIT_6 | BIT_7);
    
    mPORTBClearBits(BIT_10 | BIT_3 | BIT_2);
    mPORTEClearBits(BIT_4 | BIT_6 | BIT_7);
    
    while(1){
        
        
        if (! vu16_LED_millis)
        {
            vu16_LED_millis = 1000;
            LED_toggle (LED_2);
        }
        
        
        
        //        while ( !mCTGetIntFlag( ) );
        //        mCTClearIntFlag ( );
        //        UpdateCoreTimer (CORE_TICK_RATE);
        //        LED_alternate(LED);
        //        if (LED == 2)
        //            LED=0;
        //        else
        //            LED++;    
        
    };
}

void LED_on(uint8_t LED){
    switch (LED) {
        case LED_1:
            mPORTESetBits(BIT_4);
            break;
            
        case LED_2:
            mPORTESetBits(BIT_6);
            break;
            
        case LED_3:
            mPORTESetBits(BIT_7);
            break;
            
        default:
            break;
    }
}

void LED_off(uint8_t LED){
    switch (LED) {
        case LED_1:
            mPORTEClearBits(BIT_4);
            break;
        case LED_2:
            mPORTEClearBits(BIT_6);
            break;
        case LED_3:
            mPORTEClearBits(BIT_7);
            break;
        default:
            break;
    }
}

void LED_toggle(uint8_t LED){
    switch (LED) {
        case LED_1:
            mPORTEToggleBits(BIT_4);
            break;
            
        case LED_2:
            mPORTEToggleBits(BIT_6);
            break;
            
        case LED_3:
            mPORTEToggleBits(BIT_7);
            break;
            
        default:
            break;
    }
}

void LED_alternate(uint8_t LED){
    switch (LED) {
        case LED_1:
            mPORTESetBits(BIT_4);
            mPORTEClearBits(BIT_6 | BIT_7);
            break;            
        case LED_2:
            mPORTESetBits(BIT_6);
            mPORTEClearBits(BIT_4 | BIT_7);
            break;
        case LED_3:
            mPORTESetBits(BIT_7);
            mPORTEClearBits(BIT_6 | BIT_4);
            break;            
        default:
            break;
    }
}


void __ISR ( _CORE_TIMER_VECTOR , IPL3) CoreTimerHandler (void)
{
    mCTClearIntFlag (); // borrar el indicador de interrupci�n
    UpdateCoreTimer (CORE_TICK_RATE); // recargar el per�odo
    
    
    if (vu16_LED_millis) vu16_LED_millis--;
}